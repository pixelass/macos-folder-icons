# Change Log

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

<a name="1.1.2"></a>
## [1.1.2](https://github.com/pixelass/macos-folder-icons/compare/v1.1.1...v1.1.2) (2018-06-20)


### Bug Fixes

* **npm:** deliver required files ([4c3d86d](https://github.com/pixelass/macos-folder-icons/commit/4c3d86d)), closes [#1](https://github.com/pixelass/macos-folder-icons/issues/1)



<a name="1.1.1"></a>
## [1.1.1](https://github.com/pixelass/macos-folder-icons/compare/v1.1.0...v1.1.1) (2018-04-09)


### Bug Fixes

* **npm:** add missing files ([f548de8](https://github.com/pixelass/macos-folder-icons/commit/f548de8))



<a name="1.1.0"></a>
# [1.1.0](https://github.com/pixelass/macos-folder-icons/compare/v1.0.0...v1.1.0) (2018-04-09)


### Features

* js API ([da41a68](https://github.com/pixelass/macos-folder-icons/commit/da41a68))



<a name="1.0.0"></a>
# [1.0.0](https://github.com/pixelass/macos-folder-icons/compare/v0.4.3...v1.0.0) (2018-04-06)


### Features

* **cli:** allow ignoring files ([e74ccb3](https://github.com/pixelass/macos-folder-icons/commit/e74ccb3))


### BREAKING CHANGES

* **cli:** icon option removes alias i used for ignore



<a name="0.4.3"></a>
## [0.4.3](https://github.com/pixelass/macos-folder-icons/compare/v0.4.2...v0.4.3) (2018-04-06)


### Bug Fixes

* **cli:** flag was not respected ([55f6ddf](https://github.com/pixelass/macos-folder-icons/commit/55f6ddf))



<a name="0.4.2"></a>
## [0.4.2](https://github.com/pixelass/macos-folder-icons/compare/v0.4.1...v0.4.2) (2018-04-06)



<a name="0.4.1"></a>
## [0.4.1](https://github.com/pixelass/macos-folder-icons/compare/v0.4.0...v0.4.1) (2018-04-05)



<a name="0.4.0"></a>
# [0.4.0](https://github.com/pixelass/macos-folder-icons/compare/v0.3.0...v0.4.0) (2018-04-05)


### Features

* allow glob pattern ([c279e48](https://github.com/pixelass/macos-folder-icons/commit/c279e48))



<a name="0.3.0"></a>
# [0.3.0](https://github.com/pixelass/macos-folder-icons/compare/v0.2.0...v0.3.0) (2018-04-05)


### Features

* **icns:** create .icns file ([6712163](https://github.com/pixelass/macos-folder-icons/commit/6712163))



<a name="0.2.0"></a>
# [0.2.0](https://github.com/pixelass/macos-folder-icons/compare/v0.1.2...v0.2.0) (2018-04-05)


### Features

* **svg:** clean svgs ([210e868](https://github.com/pixelass/macos-folder-icons/commit/210e868))



<a name="0.1.2"></a>
## [0.1.2](https://github.com/pixelass/macos-folder-icons/compare/v0.1.1...v0.1.2) (2018-04-05)


### Bug Fixes

* **bin:** fixes root dir ([0d97dcf](https://github.com/pixelass/macos-folder-icons/commit/0d97dcf))



<a name="0.1.1"></a>
## [0.1.1](https://github.com/pixelass/macos-folder-icons/compare/v0.1.0...v0.1.1) (2018-04-05)


### Bug Fixes

* **bin:** missing bin field ([174d065](https://github.com/pixelass/macos-folder-icons/commit/174d065))



<a name="0.1.0"></a>
# 0.1.0 (2018-04-05)


### Features

* sizing ([236ae01](https://github.com/pixelass/macos-folder-icons/commit/236ae01))
