## Description
<!-- Enter a short description here -->

<!-- List motivation and changes here -->
## Motivation

<!-- List closed issues here -->
## Issues closed

<!-- Checklist -->
## Checklist

- [ ] Read [Code of Conduct](https://github.com/pixelass/macos-folder-icons/blob/master/.github/CODE_OF_CONDUCT.md) 
- [ ] Read [Contributing guidelines](https://github.com/pixelass/macos-folder-icons/blob/master/.github/CONTRIBUTING.md) 

