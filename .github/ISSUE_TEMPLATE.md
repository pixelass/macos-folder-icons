## Issue description
<!-- Enter a short description here -->

## Steps to reproduce
<!-- List the steps to reproduce the issue -->

1. 
1. 
1. 
1. 

## Logs
<!-- If you have a logfile please paste it here -->
<!-- If you don't have a logfile feel free to delete the details block -->

<details>
  <summary>Error log</summary>
  
```shell
## LOG
```
</details>

## Your Environment
<!--- Include as many relevant details about the environment you experienced the bug in -->

| Executable | Version |
| ---: | :--- |
| `create-mac-folder --version` | ENTER_VERSION_HERE |
| `node --version` | ENTER_VERSION_HERE |
| `npm --version` | ENTER_VERSION_HERE |
