module.exports = {
    arrowParens: 'avoid',
    parser: 'babylon',
    trailingComma: 'none',
    printWidth: 120,
	tabWidth: 4,
    requirePragma: false,
    bracketSpacing: false,
    jsxBracketSameLine: true,
    semi: true,
    singleQuote: false,
    useTabs: true
}
