module.exports = async (files, size) => {
	const HEIGHT = 1024;
	const WIDTH = 1024;

	/**
	 * Draws the image on a canvas and returns the data
	 * @param artboard
	 * @param artboard.ctx
	 * @param artboard.canvas
	 * @param folder
	 * @param icon
	 * @returns {string}
	 */
	const drawImage = ({ctx, canvas}, folder, icon) => {
		const folderFront = {
			left: 39,
			top: 258
		};
		folderFront.right = WIDTH - 37;
		folderFront.bottom = HEIGHT - 214;
		folderFront.width = folderFront.right - folderFront.left;
		folderFront.height = folderFront.bottom - folderFront.top;

		const dx = (folderFront.width - size) / 2 + folderFront.left;
		const dy = (folderFront.height - size) / 2 + folderFront.top + 18;
		const width = WIDTH - dx * 2;
		const height = width;
		// Reset artboard
		ctx.clearRect(0, 0, WIDTH, HEIGHT);
		ctx.globalAlpha = 1;
		ctx.globalCompositeOperation = "source-over";
		// Draw the folder
		ctx.drawImage(folder, 0, 0, WIDTH, HEIGHT);
		// Draw the shadow of the icon
		// Very low opacity
		ctx.globalAlpha = 0.075;
		ctx.globalCompositeOperation = "multiply";
		ctx.drawImage(icon, dx, dy, width, height);
		// Draw the overlay (shadows & lights) of the icon
		// mid opacity
		ctx.globalAlpha = 0.4;
		ctx.globalCompositeOperation = "overlay";
		ctx.drawImage(icon, dx, dy, width, height);
		// Return the imageData
		return canvas.toDataURL();
	};

	/**
	 * 	Creates an artboard at a defined size
	 */
	const createArtboard = (width = 1024, height = width) => {
		const canvas = document.createElement("canvas");
		canvas.height = height;
		canvas.width = width;
		const ctx = canvas.getContext("2d");
		return {canvas, ctx};
	};
	const artboard = createArtboard(WIDTH, HEIGHT);

	const folder = await new Promise((resolve, reject) => {
		const img = document.createElement("img");
		img.src = "folder.png";

		img.onload = e => {
			resolve(e.target);
		};
		img.onerror = err => {
			reject(err);
		};
	});

	// Wait until all icons are loaded
	// Set a timeout at 100ms
	// @todo timeout is a workaround since local SVGs don't trigger the onload event
	const icons = await Promise.all(
		files.map(file => {
			const icon = new Promise((resolve, reject) => {
				const img = document.createElement("img");
				img.src = `${file}.svg`;
				setTimeout(() => {
					resolve({file, icon: img});
				}, 100);
				img.onload = e => {
					resolve({file, icon: e.target});
				};
				img.onerror = err => {
					reject(err);
				};
			});

			return icon;
		})
	);

	// Collect data from all icons
	const data = icons.map(({file, icon}) => ({
		file,
		icon: drawImage(artboard, folder, icon)
	}));

	return data;
};
