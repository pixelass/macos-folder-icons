#!/usr/bin/env node
const path = require("path");
const pify = require("pify");
const {writeFile} = pify(require("fs"));
const mkdirp = pify(require("mkdirp"));
const rimraf = pify(require("rimraf"));
const copy = pify(require("copy"));
const globby = require("globby");
const execa = require("execa");
const puppeteer = require("puppeteer");
const ora = require("ora");

// Local modules
const {createServer, runServer} = require("./localhost");
const {leftPad, rightPad, arrToChunks, plural} = require("./utils");
const run = require("./run");
const log = require("./logger");
const writeIcns = require("./write-icns");
const setIcon = require("./set-icon");
const getSVG = require("./get-svg");
const makeSVG = require("./make-svg");

// Constants
const {PUBLIC, ROOT} = require("./constants");

// Create a spinner instance
const spinner = ora({
	text: "Preparing",
	spinner: "growVertical"
});

// Create a server instance
const server = createServer(PUBLIC);

const makeIcons = (chunk, page, {outPath, size, fullLength, chunkIndex, chunkLength, longest}) =>
	new Promise(async (resolve, reject) => {
		const {length} = chunk;
		// Adjust spinner text
		const logData = `${chunkIndex + 1} of ${chunkLength} ${plural(chunkIndex, "chunk")} | Creating ${length} ${plural(length, "folder")}`;
		spinner.color = "yellow";
		spinner.text = `Building templates: ${logData}`;

		// An array of fileNames without the path
		const files = chunk.map(x => path.parse(x).name);

		// An array ob symbols generated from the SVG files
		const symbols = (await Promise.all(chunk.map(getSVG))).map(x => ({
			file: x.file,
			data: x.data.replace(/<svg/, '<symbol id="d"').replace(/\/svg>/, "/symbol>")
		}));

		// An array of icons generated from the symbols
		const icons = symbols.map(({file, data}) => ({
			file,
			data: makeSVG(data)
		}));

		// Write all icons to the public folder
		// @todo use stream instead
		await Promise.all(
			icons.map(({file, data}) => {
				const {base} = path.parse(file);
				const outFile = path.resolve(PUBLIC, base);
				return writeFile(outFile, data);
			})
		);

		// Get return value from the run script
		const result = await page.evaluate(run, files, size);

		// Progress counter (used for spinner text)
		let c = 0;
		spinner.color = "green";
		spinner.text = `Writing icons: ${logData}`;

		// Create png and icns files
		await Promise.all(
			result.map(
				({file, icon}) =>
					new Promise(async (resolve, reject) => {
						// Resolve paths
						// Folder "might" need to be created
						// File is written after folder has been ensured.
						const folder = path.resolve(outPath, file);
						const pngPath = path.resolve(folder, `${file}.png`);
						const icnsPath = path.resolve(folder, `${file}.icns`);
						const iconset = path.resolve(folder, `${file}.iconset`);
						// Clean content before writing file
						const content = icon.replace(/^data:image\/png;base64,/, "");

						// Create icon folder
						// Write png file
						// Write icns file
						await mkdirp(folder);
						await writeFile(pngPath, content, "base64");
						await writeIcns(pngPath);
						// add icons to all folders
						await setIcon(folder, icnsPath);
						await setIcon(iconset, icnsPath);

						// Helpers (used for spinner text)
						const iOfN = `${++c + chunkIndex * length} of ${fullLength}`;
						const {length: l} = longest;
						const m = `${fullLength}`.length * 2 + 5;
						// Show success via spinner
						// logs name, i of n, folder
						spinner.succeed(`${rightPad(file, l + 1)} | ${leftPad(iOfN, m)} | ${folder}`);
						resolve();
					})
			)
		);
		spinner.start();
		resolve();
	});

module.exports = async (globPattern, {outDir, size, port}) => {
	// Start spinner
	// Start server
	// Create public folder
	// Create index.html
	// Copy the folder base to public
	spinner.start();
	spinner.color = "red";
	runServer(server, port);
	await mkdirp(PUBLIC);
	await writeFile(path.resolve(PUBLIC, "index.html"), "<!DOCTYPE html><html><body></body></html>");
	await copy(path.resolve(ROOT, "folder.png"), PUBLIC);

	// Resolve outDir
	const outPath = path.resolve(outDir);

	// Get all filenames from the globPattern
	const globFiles = await globby(globPattern);

	// Create puppeteer instance
	const browser = await puppeteer.launch({
		ignoreHTTPSErrors: true
	});
	const page = await browser.newPage();

	// Open the page and wait until the network is idle
	await page.goto(`https://localhost:${port}`, {
		waitUntil: "networkidle2"
	});

	const chunks = arrToChunks(globFiles, 20);
	// Get longest fileName (used for pretty logs)
	const [longest] = [...globFiles.map(x => path.parse(x).name)].sort((a, b) => a.length - b.length).reverse();
	for (let chunkIndex = 0; chunkIndex < chunks.length; chunkIndex++) {
		await makeIcons(chunks[chunkIndex], page, {
			outPath,
			size,
			longest,
			chunkIndex,
			chunkLength: chunks.length,
			fullLength: globFiles.length
		});
	}

	// We're done with puppeteer.
	// Close the browser and stop the server
	// Clean the public folder
	await browser.close();
	server.close();
	await rimraf(PUBLIC);
	spinner.stop();
};

const exitHandler = ({clean, exit, close}, err) => {
	spinner.stop();
	if (clean) {
		rimraf(PUBLIC);
	}
	if (close) {
		server.close();
	}
	if (err) {
		log.error(err.stack);
	}
	if (exit) {
		process.exit();
	}
};

// App is closing
process.on("exit", err => exitHandler({clean: true, close: true}, err));

// Catches ctrl+c event
process.on("SIGINT", err => exitHandler({exit: true}, err));

// Catches "kill pid" (for example: nodemon restart)
process.on("SIGUSR1", err => exitHandler({exit: true}, err));
process.on("SIGUSR2", err => exitHandler({exit: true}, err));

// Catches uncaught exceptions
process.on("uncaughtException", err => exitHandler({exit: true}, err));
