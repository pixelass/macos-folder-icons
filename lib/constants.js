const path = require("path");
const uuid = require("uuid");

const DEFAULT_PORT = 1337;
const DEFAULT_SIZE = 426;
const ROOT = process.cwd();
const PUBLIC = path.resolve(ROOT, `.${uuid()}`);
const DEFAULT_OUT_DIR = path.join(ROOT, "macos-folder-icons");

module.exports = {
	DEFAULT_OUT_DIR,
	DEFAULT_PORT,
	DEFAULT_SIZE,
	PUBLIC,
	ROOT
};
