const meow = require("meow");

/**
 * CLI setup
 * @type {{input, flags, pkg, help, showHelp, showVersion}}
 */
module.exports = meow(
	`
    Usage
    $ create-mac-folder <input>
    Options
      --ignore, -i  glob pattern to ignore 
      --out-dir, -o  output directory 
      --size, -s  size of icon 
      --port, -p  an open port on localhost 
    Examples
      $ create-mac-folder path/to/my.svg 
      $ create-mac-folder path/to/my.svg --out-dir out
      $ create-mac-folder path/to/my.svg --o out
      $ create-mac-folder 'path/to/my/icons/*.svg' -s 256 -i '_*.svg'
      $ create-mac-folder 'path/to/my/icons/*.svg' -o out -p 3000
`,
	{
		flags: {
			ignore: {
				type: "string",
				alias: "i"
			},
			size: {
				type: "number",
				alias: "s"
			},
			port: {
				type: "number",
				alias: "p"
			},
			"out-dir": {
				type: "string",
				alias: "o"
			}
		}
	}
);
