/**
 * Creates spaces to pad strings
 * @param str
 * @param maxLength
 * @returns {string}
 */
const createSpaces = (str, maxLength = 10) =>
	Array(Math.max(0, maxLength - str.length))
		.map(() => " ")
		.join(" ");

/**
 * Pads a string on the right
 * @param str
 * @param n
 * @returns {string}
 */
const rightPad = (str, n) => `${str}${createSpaces(str, n)}`;

/**
 * Pads a string on the left
 * @param str
 * @param n
 * @returns {string}
 */
const leftPad = (str, n) => `${createSpaces(str, n)}${str}`;

/**
 * Creates the plural of a word
 * @param n
 * @param str
 * @param s
 * @returns {string}
 */
const plural = (n, str, s = "s") => `${str}${n === 1 ? "" : s}`;

/**
 *
 * @param arr
 * @param size
 * @returns {Array}
 */
const arrToChunks = (arr, size) => {
	const res = [];
	for (let i = 0; i < arr.length; i += size) {
		res.push(arr.slice(i, i + size));
	}
	return res;
};


module.exports = {
	createSpaces,
	leftPad,
	rightPad,
	plural,
	arrToChunks
};
