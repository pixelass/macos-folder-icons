const path = require("path");
const execa = require("execa");
const {ROOT, PUBLIC} = require("./constants");

/**
 * Creates ICNS files from a PNG
 * @param pngFile
 * @returns {Promise<any>}
 */
module.exports = (folder, icon) =>
	new Promise((resolve, reject) => {
		execa("fileicon", ["set", folder, icon])
			.then(() => {
				resolve();
			})
			.catch(err => {
				reject(err);
			});
	});
