const winston = require("winston");
const {rightPad} = require("./utils");

/**
 * Creates a logger to report info of the script
 * @type {*|DerivedLogger}
 */
module.exports = winston.createLogger({
	level: process.env.LOG_LEVEL,
	format: winston.format.combine(
		winston.format.json(),
		winston.format.colorize(),
		winston.format.simple(),
		winston.format.printf(({level, message}) => `${rightPad(level, 20)}${message}`)
	),
	transports: [new winston.transports.Console()]
});
