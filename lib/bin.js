const build = require("./");
const log = require("./logger");
const {DEFAULT_OUT_DIR, DEFAULT_PORT, DEFAULT_SIZE} = require("./constants");
const {
	input = [],
	flags: {ignore = [], outDir = DEFAULT_OUT_DIR, size = DEFAULT_SIZE, port = DEFAULT_PORT}
} = require("./cli");

// Check if input is defined
if (input.length) {
	// Build folder from input
	const ignorePatterns = (Array.isArray(ignore) ? ignore : [ignore]).map(x => `!${x}`);
	const globPattern = input.concat(ignorePatterns);
	build(globPattern, {outDir, size, port});
} else {
	log.error("<input> is required");
}
