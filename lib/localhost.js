const path = require("path");
const {createServer} = require("http-server");

/**
 * Create a local server at a given root directory
 * @param root
 */
const createLocalHost = root =>
	createServer({
		root,
		https: {
			cert: path.resolve(__dirname, "./host.cert"),
			key: path.resolve(__dirname, "./host.key")
		},
		headers: {
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Credentials": "true"
		}
	});

/**
 * Runs the server inside a promise to prevent stalling the script
 * @param server
 * @param port
 * @returns {Promise<any>}
 */
const runServer = (server, port) =>
	new Promise((resolve, reject) => {
		server.listen(port);
		resolve();
	});

module.exports = {
	runServer,
	createServer: createLocalHost
};
