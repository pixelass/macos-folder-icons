const pify = require("pify");
const {readFile} = pify(require("fs"));
const svgo = require("./svgo");

/**
 * Reads an SVG file and returns the content and path
 * Uses SVVGO to optimize the SVG
 * @param file
 * @returns {Promise<any>}
 */
module.exports = file =>
	new Promise((resolve, reject) => {
		readFile(file, "utf-8")
			.then(content => {
				svgo
					.optimize(content, {path: file})
					.then(({data}) => {
						resolve({file, data});
					})
					.catch(err => {
						reject(err);
					});
			})
			.catch(err => {
				reject(err);
			});
	});
