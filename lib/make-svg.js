/**
 * Creates an SVG mask from a symbol
 * @param symbol
 * @returns {string}
 */
module.exports = symbol => `
<svg height="1024" width="1024" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
    <defs>
        <linearGradient id="a" x2="0" y2="1">
            <stop offset="5%" stop-color="#333"/>
            <stop offset="95%" stop-color="#555"/>
        </linearGradient>
        <filter id="b">
            <feGaussianBlur in="SourceAlpha" result="a" stdDeviation=".075"/>
            <feOffset dy=".125" in="a" result="b"/>
            <feFlood flood-color="#fff" flood-opacity="1" result="c"/>
            <feComposite in="c" in2="b" operator="in" result="b"/>
            <feComponentTransfer in="b" result="b">
                <feFuncA slope=".75" type="linear"/>
            </feComponentTransfer>
            <feMerge>
                <feMergeNode/>
                <feMergeNode in="SourceGraphic"/>
            </feMerge>
        </filter>
        <filter id="c">
            <feGaussianBlur result="a" stdDeviation=".25"/>
            <feOffset dy=".125" in="a" result="b"/>
            <feFlood flood-color="#000" flood-opacity="1" result="c"/>
            <feComposite in="SourceGraphic" in2="b" operator="out" result="d"/>
            <feComposite in="c" in2="d" operator="in" result="e"/>
            <feComponentTransfer in="e" result="e">
                <feFuncA type="linear"/>
            </feComponentTransfer>
        </filter>
        ${symbol}
    </defs>
    <use width="24" height="24" fill="url(#a)" filter="url(#b)" href="#d"/>
    <use width="24" height="24" filter="url(#c)" href="#d"/>
</svg>
`;
