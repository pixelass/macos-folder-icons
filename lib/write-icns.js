const path = require("path");
const execa = require("execa");
const {ROOT, PUBLIC} = require("./constants");

/**
 * Creates ICNS files from a PNG
 * @param pngFile
 * @returns {Promise<any>}
 */
module.exports = pngFile =>
	new Promise((resolve, reject) => {
		execa("sh", [path.resolve(ROOT, "png-to-icns.sh"), pngFile])
			.then(() => {
				resolve();
			})
			.catch(err => {
				reject(err);
			});
	});
