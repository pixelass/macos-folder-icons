#!/usr/bin/env node
module.exports = {
	default: require('./lib'),
	setIcon: require('./lib/set-icon')
}
