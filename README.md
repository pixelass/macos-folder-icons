# Create mac folder

[![npm](https://img.shields.io/npm/v/macos-folder-icons.svg?style=for-the-badge)](https://www.npmjs.com/package/macos-folder-icons)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg?style=for-the-badge)](https://raw.githubusercontent.com/pixelass/macos-folder-icons/master/LICENSE)

A cli tool to create macOS style folders from SVG files.

* 💩 No extra tools needed

Creates `.png` and `.icns` files from SVGs.
Tested with various icon types via [svg-icon](https://github.com/leungwensen/svg-icon).
`create-mac-folder` will attempt to make all SVGs compatible though there are limitations of what can be done.
Most SVGs will work just fine (See [examples](#examples) below).

## Installation

```shell
npm install --global macos-folder-icons
```

## Usage

| Option      | Alias | Type     | Default                | Description |
|------------:|:-----:|:--------:|------------------------|-------------|
| `<input>`   | n.a.  | `string` | n.a.                   | Default command |
| `--out-dir` | `-o`  | `string` | `./macos-folder-icons` | Defines the output directory |
| `--ignore`  | `-i`  | `string` | `undefined`            | Defines ignore patterns |
| `--port`    | `-p`  | `number` | `1337`                 | Change the port if `1337` is taken |
| `--size`    | `-s`  | `number` | `424`                  | Change the size of the icon viewBox |

**Core options**

| Option      | Description |
|-------------|-------------|
| `--help`    | prints help |
| `--version` | prints version |

### Single files

```shell
create-mac-folder path/to/my.svg
```

### Multiple files

```shell
create-mac-folder one.svg two.svg
```

```shell
create-mac-folder '{one,two,four}.svg'
```

```shell
create-mac-folder 'path/to/my/*.svg'
```

## Options

### Input

Input is required. It allows using multiple strings or glob patterns,

* Type: `string`

> `path/to/my.svg` --> `./macos-folder-icons/my.svg`

```shell
create-mac-folder path/to/my.svg
```

**Glob patterns**

> Make sure to put your glob patterns in quotes `'**/glob/*/pattern'`.

```shell
create-mac-folder 'path/to/my/*.svg'
```

### Out Dir

Defines the output directory relative to the current working directory (`process.cwd()`).

* Type: `string`

> `path/to/my.svg` --> `./out/my.svg`

```shell
create-mac-folder path/to/my.svg -o out
```

### Ignore

Defines files to ignore. Allows glob patterns and multiple definitions

> Make sure to put your glob patterns in quotes `'**/glob/*/pattern'`.

* Type: `string`

> `**/*.svg, !**/qux/*.svg, !foo/bar/baz.svg` --> `./macos-folder-icons/*`

```shell
create-mac-folder '**/*.svg' --ignore '**/qux/*.svg' --ignore 'foo/bar/baz.svg'
```

### Port

Change the port in case `1337` is already in use

* Type: `number`

```shell
create-mac-folder path/to/my.svg --port 3000
```

### Size

Define the size of the icon. The default is optimized to use circular icons from
[svg-icon/simple](https://github.com/leungwensen/svg-icon/tree/91facc1b8fdaea46dcab96cf479825edca374660/dist/svg/simple) 
(e.g. <img width="24" src="https://cdn.rawgit.com/leungwensen/svg-icon/926938aa64e22d178822640c5541fc7439e383fc/dist/svg/simple/github.svg" alt="The github logo" />)
	  
* Type: `number`

```shell
create-mac-folder path/to/my.svg --size 300
```



## Examples

Take a look at the [Example files](https://github.com/pixelass/macos-folder-icons/tree/master/examples/out).

### Input / Output

<p>
    <img width="256"
         src="https://cdn.rawgit.com/leungwensen/svg-icon/926938aa64e22d178822640c5541fc7439e383fc/dist/svg/simple/github.svg"
         alt="The github logo" />
    <img width="256" 
          src="https://raw.githubusercontent.com/pixelass/macos-folder-icons/master/examples/out/github/github.png"
          alt="Example folder using the github logo"/>
<p>


<p>
    <img width="256"
         src="https://cdn.rawgit.com/leungwensen/svg-icon/926938aa64e22d178822640c5541fc7439e383fc/dist/svg/logos/docker.svg"
         alt="The sass logo" /> 
    <img width="256" 
          src="https://raw.githubusercontent.com/pixelass/macos-folder-icons/master/examples/out/docker/docker.png"
          alt="Example folder using the sass logo"/>
<p>
